import * as express from 'express';
import * as cors from 'cors';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.mountRoutes();
    }

    private mountRoutes() {
        this.app.use(cors({ origin: true }));
        
        const router = express.Router();

        router.get('/', (req, res) => {
            res.json({
              message: 'Hello World!'
            });
        });

        this.app.use('/', router);
    }
}

export default new App().app;