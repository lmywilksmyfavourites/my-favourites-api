import { db } from '../config/firebase';

export class BaseController {

    protected getDoc(url: string) {
        return db.doc(url).get();
    }

    protected composeResponse(status: number, message: string, data?: any) {
        const res: any = {};

        res.response = { status, message };
        if (data) res.data = { ...data };
        
        return res;
    }
}