import { firebase, db } from '../config/firebase';
import { BaseController } from './base.controller';

export class Auth extends BaseController {

    public async Signup(req: any, res: any, next: any) {
        try {
            const newUser = { ...req.body };
        
            const noImg = 'no-img.png';
    
            const doc = await this.getDoc(`/users/${ newUser.username }`);
            
            if (doc.exists) {
                return res
                    .status(400)
                    .json({ username: 'this username is already exist'});
            }
    
            const data = await firebase
                .auth()
                .createUserWithEmailAndPassword(newUser.email, newUser.password);
    
            if (!data) return res.status(400).json(this.composeResponse(403, ''));
            
            const userId = data.user.uid;
            const token = await data.user.getIdToken();
    
            const userCredentials = {
                username  : newUser.username,
                email     : newUser.email,
                createdAt : new Date().toISOString(),
                updatedAt : new Date().toISOString(),
                role      : 'user',
                imageUrl  : `https://firebasestorage.googleapis.com/v0/b/${ FIREBASE.storageBucket }/o/${ noImg }?alt=media`,
                userId
            };
    
            await db
                .doc(`/users/${ newUser.username }`)
                .set(userCredentials);
    
            return res.status(201).json({ token });
        } catch (err) {
            console.log(err);
            if (err.code === 'auth/email-already-in-use') {
                return res.status(400).json({ email: 'Email is already in use.' });
            } else if (err.code === 'auth/weak-password') {
                return res.status(400).json({ password: 'Password should be at least 6 characters.' });
            } else {
                return res.status(500).json({ general: 'Something went wrong, please try again.' });
            }
        }
    }


    public async login(req: any, res: any, next: any) {
        try {
            const login = { ...req.body };
    
            const data = await firebase
                .auth()
                .signInWithEmailAndPassword(login.email, login.password);
    
            if (data && data.user) {
                const token = await data.user.getIdToken();
                return res.json({ token });
            }
    
            return res.status(403).json({ message: 'Wrong credentials, please try again.' });
        } catch (error) {
            console.log(error);
            // auth/wrong-password
            // auth/user-not-found
            return res
                .status(403)
                .json( { general: 'Wrong credentials, please try again.' });
        }
    }
}