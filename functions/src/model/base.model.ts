export class Base {
    createdDt: number;
    updatedDt: number;
    creator: Creator;

    constructor(obj: any) {
        this.createdDt = obj.createdDt ? obj.createdDt : new Date().getTime();
        this.updatedDt = new Date().getTime();
        this.creator = new Creator(obj.creator);
    }
}

export class Creator {
    userId: string;
    userName: string;
    avatar: string;
    
    constructor(obj?: any) {
        this.userId = obj.userId || '';
        this.userName = obj.userName || '';
        this.avatar = obj.avatar || '';
    }
}