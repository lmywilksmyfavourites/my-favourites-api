import { Base } from './base.model';

export class User extends Base {
    private static readonly collections: string = 'users';

    userId?: string;
    userName: string;
    email: string;
    password: string;
    confirmPassword: string;

    userDetail?: UserDetail;

    constructor(obj: any) {
        super(obj);
    }
}

export class UserDetail {
    firstName: string;
    lastName: string;
    phone: string;
    address1: string;
    address2: string;
    city: string;
    province: string;
    postalCode: string;
    country: string;
}