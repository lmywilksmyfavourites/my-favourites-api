import { admin, db } from '../config/firebase';

export const isAuthorized = async (req: any, res: any, next: any) => {
    try {
        let idToken: string;

        if (
            req.headers.authorization && 
            req.headers.authorization.startsWith('Bearer ')
        ) {
            idToken = req.headers.authorization.split('Bearer ')[1];
        } else {
            console.error('No token found.')
            return res.status(401).json({ error: 'Unauthorized' })
        }

        const decodedToken = await admin
            .auth()
            .verifyIdToken(idToken);

        req.user = decodedToken;
        
        const data = await db
            .collection('users')
            .where('userId', '==', req.user.uid)
            .limit(1)
            .get();

        if (data && data.docs && data.docs[0] && data.docs[0].data()) {
            req.user.username = data.docs[0].data().username;
            req.user.imageUrl = data.docs[0].data().imageUrl;
            return next();
        }

        return res.status(401).json({ error: 'User not found.' });
    } catch (error) {
        console.error('Error while verifying token: ', error);
        return res.status(401).json(error);
    }
}