import * as firebase_admin from 'firebase-admin';
import * as firebase_root from 'firebase';
import { Firebase } from './config';

const serviceAccount = require('./service_account.json');

export const firebase = firebase_root.initializeApp(Firebase);

export const admin = firebase_admin.initializeApp({
    credential    : firebase_admin.credential.cert(serviceAccount),
    databaseURL   : "https://my-favourites-e9d6f.firebaseio.com",
    storageBucket : "my-favourites-e9d6f.appspot.com"
});

export const db = firebase_admin.firestore();