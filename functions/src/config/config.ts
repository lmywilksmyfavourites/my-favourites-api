export const Firebase = {
    apiKey: "AIzaSyBwJV7sG9yHp7UFBSfr9J8dRMRFMkwm_PA",
    authDomain: "my-favourites-e9d6f.firebaseapp.com",
    databaseURL: "https://my-favourites-e9d6f.firebaseio.com",
    projectId: "my-favourites-e9d6f",
    storageBucket: "my-favourites-e9d6f.appspot.com",
    messagingSenderId: "402468352698",
    appId: "1:402468352698:web:a054b64d52b077df1e8883",
    measurementId: "G-VKECN0Z922"
};

export const StorageBucketPath = (path: string) => {
    const root = 'https://firebasestorage.googleapis.com/v0/b/my-favourites-e9d6f.appspot.com/o/';
    return root + path;
}

